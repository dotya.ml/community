# [community](https://git.dotya.ml/dotya.ml/community)

platform and community related issues

[![Matrix](https://img.shields.io/matrix/dotya.ml-general:matrix.org?label=%23dotya.ml-general%3Amatrix.org&style=flat-square)](https://matrix.to/#/#dotya.ml-general:matrix.org)
[![IRC](https://img.shields.io/badge/irc-%23dotya.ml-brightgreen.svg?style=flat-square)](https://webchat.freenode.net/?channels=dotya.ml)
[![Discord](https://img.shields.io/discord/778012033547239465?color=blueviolet&label=discord&style=flat-square)](https://discord.gg/s55tZUG2rR)
