---
name: "Proposal :bulb:"
title: "[Proposal] "
about: "A proposal :bulb: template"
labels:
  - proposal
---
<!--
 This is a proposal template.
 Please, describe what your idea is trying to solve, who is it going to benefit,
 how do you imagine it gets done and how you yourself could contribute.

 Delete this commented section before posting.
-->
